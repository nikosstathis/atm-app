import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class AtmService {

	public SPRING_BOOT_BASE_PATH = '//localhost:8081/atmdispenser/rest/'

	constructor(private http: HttpClient) {
	}

	getNoteInformation(): Observable<any> {
		return this.http.get(this.SPRING_BOOT_BASE_PATH+'noteinformation');
	}

	withdraw(amount: any){
		return this.http.put(this.SPRING_BOOT_BASE_PATH+'withdraw', amount);
	}

	initialize(initializationDto: any){
		return this.http.post(this.SPRING_BOOT_BASE_PATH+'initialize', initializationDto);
	}
  
}