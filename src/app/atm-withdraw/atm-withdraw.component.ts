import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AtmService } from '../shared/atm/atm.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-atm-withdraw',
  templateUrl: './atm-withdraw.component.html',
  styleUrls: ['./atm-withdraw.component.scss']
})
export class AtmWithdrawComponent implements OnInit {
	response: any;
	sub: Subscription;

	constructor(private route: ActivatedRoute,
				private router: Router,
				private atmService: AtmService) { 
	}

	ngOnInit() {

	}

	withdraw(form: NgForm){
		this.atmService.withdraw(form).subscribe(
			data => {
				this.response = data; 
			},  
			error => {
				this.response = error.error;
			}
		);
	}

}
