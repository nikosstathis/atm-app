import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { AtmService } from '../shared/atm/atm.service';
import { NgForm } from '@angular/forms';

@Component({
	selector: 'app-atm-initialize',
	templateUrl: './atm-initialize.component.html',
	styleUrls: ['./atm-initialize.component.scss']
})
export class AtmInitializeComponent implements OnInit {

	response: any;
	sub: Subscription;

	constructor(private route: ActivatedRoute,
				private router: Router,
				private atmService: AtmService) { 
	}

	ngOnInit() {

	}

	initialize(form: any){
		var initializationRequest = [{
			note: "NOTE_20_EURO",
			quantity: form.amountOf20Notes
		},{
			note: "NOTE_50_EURO",
			quantity: form.amountOf50Notes
		}];
		this.atmService.initialize(initializationRequest).subscribe(
			data => {
				this.response = data; 
			},  
			error => {
				this.response = error.error;
			}
		);
	}
}

