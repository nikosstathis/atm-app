import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmInitializeComponent } from './atm-initialize.component';

describe('AtmInitializeComponent', () => {
  let component: AtmInitializeComponent;
  let fixture: ComponentFixture<AtmInitializeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmInitializeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmInitializeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
