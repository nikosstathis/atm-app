import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtmInitializeComponent } from './atm-initialize/atm-initialize.component';
import { AtmNoteinfoComponent } from './atm-noteinfo/atm-noteinfo.component';
import { AtmWithdrawComponent } from './atm-withdraw/atm-withdraw.component';

const routes: Routes = [
  {
    path: 'atm-initialize',
    component: AtmInitializeComponent
  }, 
  {
    path: 'atm-noteinfo',
    component: AtmNoteinfoComponent
  }, 
  {
    path: 'atm-withdraw',
    component: AtmWithdrawComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
