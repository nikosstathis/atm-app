import { Component, OnInit } from '@angular/core';
import { AtmService } from '../shared/atm/atm.service';

@Component({
  selector: 'app-atm-noteinfo',
  templateUrl: './atm-noteinfo.component.html',
  styleUrls: ['./atm-noteinfo.component.scss']
})
export class AtmNoteinfoComponent implements OnInit {
	response: any

	constructor(private atmService: AtmService) { }

	ngOnInit() {
		this.noteinformation();
	}
	
	noteinformation(){
		this.atmService.getNoteInformation().subscribe(
			data => {
				this.response = data; 
			},  
			error => {
				this.response = error.error;
			}
		);
	}
}
  