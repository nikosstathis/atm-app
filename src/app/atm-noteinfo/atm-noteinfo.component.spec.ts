import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmNoteinfoComponent } from './atm-noteinfo.component';

describe('AtmNoteinfoComponent', () => {
  let component: AtmNoteinfoComponent;
  let fixture: ComponentFixture<AtmNoteinfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmNoteinfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmNoteinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
